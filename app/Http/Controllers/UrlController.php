<?php

namespace App\Http\Controllers;

use App\Models\url;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Support\Facades\DB;//Trabajar con base de datos utilizando procedimientos almacenados
use Illuminate\Http\Request;//Recupera los datos de la vista
use Yajra\DataTables\DataTables;

class UrlController extends Controller
{
    //Por medio de Eloquent
    public function verUrl(Request $request){
        if($request->ajax()){
            $url = url::all();
            return DataTables::of($url)
            ->make(true);
        }
        return view('urls');
    }

    //Por medio de Query Builder
    /*
    public function verUrl(Request $request){
        if($request->ajax()){
            $url = DB::select('SELECT * FROM `urls`');
            return DataTables::of($url)
            ->make(true);
        }
        return view('urls');
    }
    */
}
